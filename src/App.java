import models.Circle;
import models.Rectangle;
import models.Square;

public class App {
    public static void main(String[] args) throws Exception {
        Rectangle rectangle1 = new Rectangle(10, 20, "red",true);
        System.out.println(rectangle1);
        System.out.println("diện tích=" + rectangle1.getArea());
        System.out.println("chu vi=" + rectangle1.getPerimeter());

        Square square1 = new Square(10, "red", true);
        System.out.println(square1);
        System.out.println("diện tích=" + square1.getArea());
        System.out.println("chu vi=" + square1.getPerimeter());

        Circle circle1 = new Circle(2, "blue", true);
        System.out.println(circle1);
        System.out.println("diện tích=" + circle1.getArea());
        System.out.println("chu vi=" + circle1.getPerimeter());
    }
}
